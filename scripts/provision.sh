#!/bin/bash
#
# This script is executed upon the first `vagrant up` or when running
# `vagrant up --provision`. You may use it to install any additional software
# that you require for your submission.
#
#
set -e

# Example

#apt-get update
#apt-get install tree
apt-get update
apt-get install python-pip
apt-get install libapache2-mod-wsgi
pip install -U pip
pip install Markdown
pip install testresources
if [ -e /usr/lib/python2.7/dist-packages/chardet-2.0.1.egg-info ]; then
    sudo rm /usr/lib/python2.7/dist-packages/chardet-2.0.1.egg-info
fi
if [ -d /usr/lib/python2.7/dist-packages/chardet ]; then
    sudo rm -r /usr/lib/python2.7/dist-packages/chardet
fi
if [ -e /usr/lib/python2.7/dist-packages/urllib3-1.7.1.egg-info ]; then
    sudo rm /usr/lib/python2.7/dist-packages/urllib3-1.7.1.egg-info
fi
if [ -d /usr/lib/python2.7/dist-packages/urllib3 ]; then
    sudo rm -r /usr/lib/python2.7/dist-packages/urllib3
fi
if [ -e /usr/lib/python2.7/dist-packages/requests-2.2.1.egg-info ]; then
    sudo rm /usr/lib/python2.7/dist-packages/requests-2.2.1.egg-info
fi
if [ -d /usr/lib/python2.7/dist-packages/requests ]; then
    sudo rm -r /usr/lib/python2.7/dist-packages/requests
fi
pip install -r /var/www/html/cisco_url_shortener/requirements.txt
# Add ServerName to apache2.conf
echo "ServerName localhost" >> /etc/apache2/apache2.conf
# Set up hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
		
		WSGIDaemonProcess cisco_url_shortener python-path=/var/www/html/cisco_url_shortener
		WSGIProcessGroup cisco_url_shortener
		WSGIScriptAlias / /var/www/html/cisco_url_shortener/cisco_url_shortener/wsgi.py

		<Directory /var/www/html/cisco_url_shortener/cisco_url_shortener>
		    <Files wsgi.py>
		        Require all granted
		    </Files>
		</Directory>

		Alias /favicon.ico /var/www/html/cisco_url_shortener/url_shortener/static/cisco_favicon.ico
		Alias /static/ /var/www/html/cisco_url_shortener/url_shortener/static/

		<Directory /var/www/html/cisco_url_shortener/url_shortener/static>
		    Require all granted
		</Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

(cd /var/www/html/cisco_url_shortener/; python manage.py collectstatic --no-input --settings=cisco_url_shortener.settings)

a2enmod rewrite
sudo service apache2 reload